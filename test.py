#!/usr/bin/env python
# -*- coding: utf-8 -*-

import config
#import jango
import telegram
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

wallet_food = 0
wallet_car = 0
wallet_home = 0
#bot = telegram.Bot(config.token)
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, TypeHandler
from telegram import (ReplyKeyboardMarkup, InlineQueryResultArticle, InputTextMessageContent)

update_id = None
MENU, INPUT, OUTPUT, STAT = range(4)

def start(bot, update):
    #user = update.message.text
    print("started")
    reply_keyboard = telegram.ReplyKeyboardMarkup([['Расход', 'Приход'],['Баланс', 'Выход']])
    bot.sendMessage(chat_id=update.message.chat_id, text="Здравствуйте. Выберите интересующее вас действие:", reply_markup=reply_keyboard)
    global update_id
    for update in bot.getUpdates(offset=update_id, timeout=10):
        update_id = update.update_id + 1
        if update.message:
            user = update.message.text
            print(user)
            #type(user)
    if user == u'Доход':
        print("доход...")
        bal_in()
    elif user == u'Расход':
        print("расход...")
        bal_out()
    elif user == u'Баланс':
        print("баланс...")
        stat()
    elif user == u'Выход':
        print("выход...")
        cancel()
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text="Здравствуйте. Выберите интересующее вас действие, нажатием на соответствующую кнопку:")
    #print(user),dir(user)

def bal_in(bot, update):
    #user = update.message.text
    print("Начато внесение доходов")
    reply_keyboard = telegram.ReplyKeyboardMarkup([['Пища', 'Дом', 'Машина'],['Назад']])
    bot.sendMessage(chat_id=update.message.chat_id, text="Выберите тип дохода, или вернитесь в меню:", reply_markup=reply_keyboard)
    global update_id
    global wallet_food
    global wallet_car
    global wallet_home
    for update in bot.getUpdates(offset=update_id, timeout=10):
        update_id = update.update_id + 1
        if update.message:
            user = update.message.text
            print(user)
    if user == u'Пища':
        for update in bot.getUpdates(offset=update_id, timeout=10):
            update_id = update.update_id + 1
            if update.message:
                user = update.message.text
                print(user)
                print("пища...")
                wallet = wallet_food + user
    elif user == u'Дом':
        for update in bot.getUpdates(offset=update_id, timeout=10):
            update_id = update.update_id + 1
            if update.message:
                user = update.message.text
                print(user)
                wallet_home = wallet_home + user
                print(wallet_home)
    elif user == u'Машина':
        for update in bot.getUpdates(offset=update_id, timeout=10):
            update_id = update.update_id + 1
            if update.message:
                user = update.message.text
                print(user)
                wallet_car = wallet_car + user
                print(wallet_car)
    elif user == u'Назад':
        print("меню...")
        start()
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text="Здравствуйте. Выберите интересующее вас действие, нажатием на соответствующую кнопку:")
    #print(user),dir(user)

def bal_out(bot, update):
    #user = update.message.text
    print("Начато внечсение расходов")
    reply_keyboard = telegram.ReplyKeyboardMarkup([['Пища', 'Дом', 'Машина'],['Назад']])
    bot.sendMessage(chat_id=update.message.chat_id, text="Выберите тип расхода, или вернитесь в меню:", reply_markup=reply_keyboard)
    global update_id
    global wallet_food
    global wallet_car
    global wallet_home
    for update in bot.getUpdates(offset=update_id, timeout=10):
        update_id = update.update_id + 1
        if update.message:
            user = update.message.text
            print(user)
    if user == u'Пища':
        print("пища...")
        for update in bot.getUpdates(offset=update_id, timeout=10):
            update_id = update.update_id + 1
            if update.message:
                user = update.message.text
                wallet_food = wallet_food - user
                print(wallet_food)
    elif user == u'Дом':
        for update in bot.getUpdates(offset=update_id, timeout=10):
            update_id = update.update_id + 1
            if update.message:
                user = update.message.text
                print(user)
                wallet_home = wallet_home - user
                print(wallet_home)
    elif user == u'Машина':
        for update in bot.getUpdates(offset=update_id, timeout=10):
            update_id = update.update_id + 1
            if update.message:
                user = update.message.text
                print(user)
                wallet_car = wallet_car - user
                print(wallet_car)
    elif user == u'Назад':
        print("меню...")
        start()
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text="Здравствуйте. Выберите интересующее вас действие, нажатием на соответствующую кнопку:")
    #print(user),dir(user)

def stat(bot, update):
    #user = update.message.text
    reply_keyboard = telegram.ReplyKeyboardMarkup([['Назад']])
    global update_id
    global wallet_food
    global wallet_car
    global wallet_home
    balance = wallet_food + wallet_car + wallet_home
    bot.sendMessage(chat_id=update.message.chat_id, text="Ваш баланс: %d" % (balance))
    start()

def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation." % user.first_name)
    bot.sendMessage(chat_id=update.message.chat_id, text="Bye! I hope we can talk again some day.")
    updater.stop()

#def main():
updater = Updater(config.token)
dp = updater.dispatcher
    #conv_handler = MessageHandler(
    #    entry_points=[MessageHandler('start', start)],
    #    states={
    #        MENU: [MessageHandler(Filters.text, start)],
    #        INPUT: [MessageHandler(Filters.text, input)],
    #        OUTPUT: [MessageHandler(Filters.text, output)],
    #        STAT: [MessageHandler(Filters.text, stat)]
    #    }
    #)
dp.addHandler(CommandHandler('start', start))
dp.addHandler(MessageHandler([Filters.text], bal_in))
dp.addHandler(MessageHandler([Filters.text], bal_out))
dp.addHandler(MessageHandler([Filters.text], stat))
dp.addHandler(CommandHandler('stop', cancel))
    #dp.add_handler(conv_handler)
    #dp.addErrorHandler(error)
updater.start_polling()
updater.idle()

    #updater = Updater(token=config.token)
    #start_handler = CommandHandler('start', start)
    #updater.dispatcher.addHandler(start_handler)
    #updater.start_polling()  # поехали!
    #updater.idle()
